/// mai js for handle event and data

// this will be run if all of the document was rendered
$(document).ready(function(){

    /// show contact modal
  $(".email").click(function (e) { 
      e.preventDefault();
      console.log("hello");
      $(".modal-container").show(500);
  });

  $(".ig").click(function (e) { 
    e.preventDefault();
    window.open("https://instagram.com/ruangprojectrp");
  });
  $(".wa").click(function (e) { 
    e.preventDefault();
    window.open("https://wa.me/6287866917340");
  });

  /// hide contact modal
  $(".close-btn").click(function (e) { 
      e.preventDefault();
       $(".modal-container").hide(500);
  });

  // back button
  $("#back-btn").click(function (e) { 
    e.preventDefault();
    location.href ="/";
  });

});