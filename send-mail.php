<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Send Email</title>
    <link rel="shortcut icon" href="assets/logo.jpg" type="image/x-icon">
    <!-- Import some source -->
    <link rel="stylesheet" href="src/css/style.css">
</head>


<?php

// send mail
// get all variable data 

$firtsName = $_POST["FIRST_NAME"];
$lastName = $_POST["LAST_NAME"];
$mailAddress = $_POST["EMAIL_ADDRESS"];
$subject = $_POST["MAIL_SUBJECT"];
$message = $_POST["MAIL_MESSAGE"];


// send the email
// use SMTP
ini_set('display_errors', 1);
error_reporting(E_ALL);
$from = $mailAddress;
$to = "info@ruangproject.id";
$subject = $subject;
$message = $message;
$headers = "From:" . $from;
mail($to, $subject, $message, $headers);
?>


<body>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="row">
                    <h2 class="title" style="text-align: center;">
                        Hurray
                        <span class="accent">
                            <?php
                            echo ($firtsName);
                            ?> </span>, Email sudah terkirim
                    </h2>
                </div>
                <div class="row justify-center">
                    <button class="btn contact-btn" id="back-btn">
                        <span>Kembali</span>
                    </button>
                </div>
            </div>
            <div class="col">
                <div class="illustartion-mail-send">
                    <img src="assets/illustration.png" alt="">
                </div>
            </div>
        </div>

    </div>

    <!-- Script source here -->
    <script src="src/js/jquery.js"></script>
    <script src="src/js/main.js"></script>
</body>

</html>