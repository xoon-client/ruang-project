<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ruang Project | Online Creative Digital Creators</title>
    <link rel="shortcut icon" href="assets/logo.jpg" type="image/x-icon">

    <!-- seo meta tags -->
    <meta name="robots" content="index, follow">
    <meta name="description" content="Creative digital creators, bantu kamu wujudkan impian digital kamu" />
    <meta name="google" content="nositelinkssearchbox" />
    <meta name="keywords" content="Digital, Image, Illustration, Ruang Project, Rp, Creators, Video, Animation, Photography, Bali" />

    <!-- facebook open graph tags -->
    <meta property="og:url" content="https://ruangproject.id" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Ruang Project" />
    <meta property="og:description" content="Creative digital creators, bantu kamu wujudkan impian digital kamu" />
    <meta property="og:image" content="https://ruangproject.id/assets/abu.png" />

    <!-- twitter meta tags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Ruang Project">
    <meta name="twitter:description" content="Creative digital creators, bantu kamu wujudkan impian digital kamu">
    <meta name="twitter:image:src" content="https://ruangproject.id/assets/abu.png">

    <!-- Import some source -->
    <link rel="stylesheet" href="src/css/style.css">
</head>

<body>

    <!-- body no header -->
    <div class="container">
        <div class="row justify-center">
            <div class="col col-2">

                <div class="row justify-center">
                    <div class="circle_image">
                        <img src="assets/abu.png" alt="Ruang Project" class="logo">
                    </div>
                </div>
                <div class="row justify-center">
                    <h2 class="title" style="text-align: center;">
                        <span class="accent"> Ruang Project,</span> Creative Digital Creators
                    </h2>
                </div>
                <div class="row justify-center">
                    <span class="desc" style="text-align: center;">
                        Ruang project hadir untuk bantu kamu wujudkan video, illustrasi,
                        presentasi, animasi dan hal menarik lainnya di dunia digital, Yuk Contact Sekarang
                    </span>
                </div>
                <div class="row justify-center" style="margin-top: 40px;">
                    <button class="social-btn email">
                        <img src="assets/icons/message.svg" alt="Whatsapp" class="btn-icon">
                        <span>Email</span>
                    </button>
                    <button class="social-btn wa">
                        <img src="assets/icons/whatsapp.svg" alt="Whatsapp" class="btn-icon">
                        <span>Whatsapp</span>
                    </button>
                    <button class="social-btn ig">
                        <img src="assets/icons/instagram.svg" alt="Whatsapp" class="btn-icon">
                        <span>Instagram</span>
                    </button>
                </div>
            </div>
        </div>
    </div>


    <!-- modal here -->
    <div class="modal-container">
        <div class="modal" id="contact-modal">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="close-btn" aria-hidden="true">
                        x
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col">
                            <h4 class="modal-title">Contact Us</h4>
                            <div class="form-container">
                                <form action="send-mail.php" method="post" class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label for="first-name" class="form-control-label">Nama Depan</label>
                                            <input type="text" name="FIRST_NAME" id="first-name" class="form-control" required>
                                        </div>
                                        <div class="col">
                                            <label for="last-name" class="form-control-label">Nama Belakang</label>
                                            <input type="text" name="LAST_NAME" id="last-name" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label for="email-address" class="form-control-label">Alamat email</label>
                                            <input type="text" name="EMAIL_ADDRESS" id="email-address" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label for="subject" class="form-control-label">Subject</label>
                                            <input type="text" name="MAIL_SUBJECT" id="subject" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <label for="message" class="form-control-label">Pesan</label>
                                            <textarea type="text" name="MAIL_MESSAGE" id="message" class="text-area-form form-control" required>
                                        </textarea>
                                        </div>
                                    </div>
                                    <div class="row justify-end">
                                        <button class="btn contact-btn" type="submit">
                                            Kirim
                                        </button>
                                    </div>
                                </form>
                            </div>


                        </div>

                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>


    <!-- Script source here -->
    <script src="src/js/jquery.js"></script>
    <script src="src/js/main.js"></script>
</body>

</html>